﻿using BabbyFriends.Enums;
using System.Xml.Linq;

namespace BabbyFriends
{
	public class Category
	{
		public XElement CreateParentCategory(CategoryParams category)
		{
			return new XElement(Attributes.category.ToString(),
				new XAttribute(Attributes.id.ToString(), category.Id),
				new XAttribute(Attributes.parentCategoryId.ToString(), category.ParentCategoryId),
				category.NameCategory);
		}
		public XElement CreateCategory(CategoryParams category)
		{
			return new XElement(Attributes.category.ToString(),
				new XAttribute(Attributes.id.ToString(), category.Id), 
				category.NameCategory);
		}
	}

	public class CategoryParams
	{
		public string Id       { get; set; }
		public string ParentCategoryId { get; set; }
		public string NameCategory     { get; set; }
	}
}
