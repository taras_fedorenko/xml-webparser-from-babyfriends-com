﻿using BabbyFriends.Enums;
using System.Collections.Generic;
using System.Xml.Linq;

namespace BabbyFriends.Offers
{
	public class Offer
	{
		public XElement CreateOffer(OfferParams offer)
		{
			var _offer = new XElement(Attributes.offer.ToString(),
				new XAttribute(Attributes.id.ToString(), offer.OfferId),
				new XAttribute(Attributes.available.ToString(), offer.Available),
				new XElement(Attributes.url.ToString(), offer.Url),
				new XElement(Attributes.price.ToString(), offer.Price),
				new XElement(Attributes.currencyId.ToString(), offer.CurrencyId),
				new XElement(Attributes.categoryId.ToString(), offer.CategoryId),
				new XElement(Attributes.name.ToString(), offer.Name),
				new XElement(Attributes.vendor.ToString(), offer.Vendor),
				new XElement(Attributes.description.ToString(), offer.Description),
				new XElement(Attributes.stockQuantity.ToString(), offer.StockQuantity)
				);

			foreach (var element in offer.Picture)
			{
				_offer.Add(new XElement(Attributes.picture.ToString(), element));
			}

			foreach (var element in offer.Param)
			{
				_offer.Add(new XElement(Attributes.param.ToString(),
					new XAttribute(Attributes.name.ToString(), element.Key), element.Value));
			}

			return _offer;
		}
	}

	public class OfferParams
	{
		public string Url { get; set; }
		public string Price { get; set; }
		public string CurrencyId { get; set; }
		public string CategoryId { get; set; }
		public List<string> Picture { get; set; }
		public string Name { get; set; }
		public string Vendor { get; set; }
		public string Description { get; set; }
		public Dictionary<string, string> Param { get; set; }
		public string StockQuantity { get; set; }
		public string OfferId { get; set; }
		public string Available { get; set; }
	}
}